<?php

$db = new mysqli(
    conf('db_host'),
    conf('db_user'),
    conf('db_password'),
    conf('db_name')
);

if ($db->connect_error) {
    exit($db->connect_error);
}

$db->set_charset(conf('db_charset'));
