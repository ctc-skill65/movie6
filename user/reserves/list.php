<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$page_path = '/user/reserves/list.php';

$items = db_result("SELECT * FROM `reserve_action` 
INNER JOIN `movie_times` ON `movie_times`.`movie_time_id`=`reserve_action`.`movie_time_id`
INNER JOIN `movies` ON `movies`.`movie_id`=`movie_times`.`movie_id`
WHERE `reserve_action`.`user_id`='{$user_id}'");

foreach ($items as &$item) {
    $seats = db_result("SELECT `theater_seats`.* FROM `reserve_items`
    LEFT JOIN `theater_seats` ON `theater_seats`.`theater_seat_id`=`reserve_items`.`theater_seat_id`
    WHERE `reserve_items`.`reserve_action_id`='{$item['reserve_action_id']}'");

    $seat_names = [];
    foreach ($seats as $seat) {
        $seat_names[] = $seat['seat_name'];
    }

    $item['seats'] = $seat_names;
    unset($item);
}

ob_start();
?>
<?= showAlert() ?>
<table>
    <thead>
        <tr>
            <th>รหัสการจอง</th>
            <th>รหัสเวลาฉาย</th>
            <th>วันเวลาเริ่มฉาย</th>
            <th>รหัสภาพยนตร์</th>
            <th>ชื่อภาพยนตร์</th>
            <th>ที่นั้งที่จอง</th>
            <th>สถานะ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
        <tr>
            <td><?= $item['reserve_action_id'] ?></td>
            <td><?= $item['movie_time_id'] ?></td>
            <td><?= $item['start_time'] ?></td>
            <td><?= $item['movie_id'] ?></td>
            <td><?= $item['name'] ?></td>
            <td><?= implode(', ', $item['seats']) ?></td>
            <td>
                <?php
                switch ($item['status']) {
                    case '-1':
                        echo 'ปฏิเสธ';
                        break;
                    
                    case '1':
                        echo 'อนุมัติ';
                        break;

                    case '0':
                        echo 'รออนุมัติ';
                        break;
                }
                ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'รายการประวัติการจองที่นั้งโรงภาพยนตร์';
require ROOT . '/user/layout.php';
