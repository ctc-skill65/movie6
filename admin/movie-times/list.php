<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/movie-times/list.php';

$action = get('action');
$id = get('id');
$sql = null;

switch ($action) {
    case 'delete':
        $sql = "DELETE FROM `movie_times` WHERE `movie_time_id`='{$id}'";
        break;
}

if ($sql) {
    $db->query($sql);
    redirect($page_path);
}

$items = db_result("SELECT * FROM `movie_times` 
INNER JOIN `movies` ON `movies`.`movie_id`=`movie_times`.`movie_id`");

ob_start();
?>
<?= showAlert() ?>
<table>
    <thead>
        <tr>
            <th>รหัสเวลาฉาย</th>
            <th>วันเวลาเริ่มฉาย</th>
            <th>รหัสภาพยนตร์</th>
            <th>โปสเตอร์</th>
            <th>ชื่อภาพยนตร์</th>
            <th>จัดการเวลาฉาย</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
        <tr>
            <td><?= $item['movie_time_id'] ?></td>
            <td><?= $item['start_time'] ?></td>
            <td><?= $item['movie_id'] ?></td>
            <td>
                <img src="<?= url($item['poster']) ?>" alt="" style="
                    max-width: 8rem;
                ">
            </td>
            <td><?= $item['name'] ?></td>
            <td>
                <a href="?action=delete&id=<?= $item['movie_time_id'] ?>" <?= clickConfirm("คุณต้องการลบเวลาฉายภาพยนตร์นี้ หรือไม่") ?>>ลบ</a>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'รายการเวลาฉายภาพยนตร์';
require ROOT . '/admin/layout.php';
