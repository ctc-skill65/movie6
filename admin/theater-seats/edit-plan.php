<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = "/admin/theater-seats/edit-plan.php";

    
if (!empty($_FILES['img']['name'])) {
    $img = upload('img', '/storage/plan');
    if (!$img) {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถอัพโหลดภาพผังที่นั้งโรงภาพยนตร์ได้");
        redirect($page_path);
    }

    $qr = $db->query("INSERT INTO `theater_plan`(`img`) VALUES ('{$img}')");
    if ($qr) {
        setAlert('success', "แก้ไขผังที่นั้งโรงภาพยนตร์สำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถแก้ไขผังที่นั้งโรงภาพยนตร์ได้");
    }

    redirect($page_path);
}

$data = db_row("SELECT * FROM `theater_plan` ORDER BY `id` DESC");

ob_start();
?>
<?= showAlert() ?>
<form method="post" enctype="multipart/form-data">
    <label for="img">
        <img src="<?= url($data['img']) ?>" alt="" style="
            max-width: 50rem;
        ">
    </label>
    <br>
    <label for="img">ภาพผังที่นั้งโรงภาพยนตร์</label>
    <input type="file" name="img" id="img" accept="image/*">
    <br>
    <button type="submit">บันทึก</button>
</form>
<?php
$layout_page = ob_get_clean();
$page_name = 'แก้ไขผังที่นั้งโรงภาพยนตร์';
require ROOT . '/admin/layout.php';
