<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/movie-times/add.php';

if (post()) {
    $qr = $db->query("INSERT INTO `movie_times`(
    `movie_id`, 
    `start_time`) VALUES (
    '{$_POST['movie']}',
    '{$_POST['start_time']}')");
    if ($qr) {
        setAlert('success', "เพิ่มเวลาฉายภาพยนตร์สำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพิ่มเวลาฉายภาพยนตร์ได้");
    }

    redirect($page_path);
}

$items = db_result("SELECT * FROM `movies`");

ob_start();
?>
<?= showAlert() ?>
<form method="post">
    <label for="movie">เลือกภาพยนตร์</label>
    <select name="movie" id="movie" required>
        <option value="" selected disabled>---- เลือก ----</option>
        <?php foreach ($items as $item) : ?>
            <option value="<?= $item['movie_id'] ?>">(#<?= $item['movie_id'] ?>) <?= $item['name'] ?></option>
        <?php endforeach; ?>
    </select>
    <br>
    <label for="start_time">วันเวลาเริ่มฉายภาพยนตร์</label>
    <input type="datetime-local" name="start_time" id="start_time" required>
    <br>
    <button type="submit">บันทึก</button>
</form>
<?php
$layout_page = ob_get_clean();
$page_name = 'เพิ่มเวลาฉายภาพยนตร์';
require ROOT . '/admin/layout.php';
