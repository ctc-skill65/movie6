<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/theater-seats/add.php';

if (post()) {
    $qr = $db->query("INSERT INTO `theater_seats`(`seat_name`) VALUES ('{$_POST['seat_name']}')");
    if ($qr) {
        setAlert('success', "เพิ่มที่นั้งโรงภาพยนตร์สำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถเพิ่มที่นั้งโรงภาพยนตร์ได้");
    }

    redirect($page_path);
}


ob_start();
?>
<?= showAlert() ?>
<form method="post">
    <label for="seat_name">ชื่อที่นั้งโรงภาพยนตร์</label>
    <input type="text" name="seat_name" id="seat_name" required>
    <br>
    <button type="submit">บันทึก</button>
</form>
<?php
$layout_page = ob_get_clean();
$page_name = 'เพิ่มที่นั้งโรงภาพยนตร์';
require ROOT . '/admin/layout.php';
