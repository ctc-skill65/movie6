<?php
require_once __DIR__ . '/../boot.php';

session_destroy();
session_start();

setAlert('success', 'ออกจากระบบสำเร็จเรียบร้อย');
redirect('/auth/login.php');
