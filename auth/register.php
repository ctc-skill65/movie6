<?php
require_once __DIR__ . '/../boot.php';

$page_path = '/auth/register.php';

if (post()) {
    $check = db_result("SELECT * FROM `users` WHERE `email`='{$_POST['email']}'");
    if (!empty($check)) {
        setAlert('error', "มีอีเมล {$_POST['email']} แล้วไม่สามารถสมัครซ้ำได้");
        redirect($page_path);
    }

    $hash = md5(post('password'));
    $qr = $db->query("INSERT INTO `users`( 
    `firstname`, 
    `lastname`, 
    `email`, 
    `password`, 
    `user_type`, 
    `status`) VALUES (
    '{$_POST['firstname']}',
    '{$_POST['lastname']}',
    '{$_POST['email']}',
    '{$hash}',
    'user',
    1)");

    if ($qr) {
        setAlert('success', 'สมัครสมาชิกสำเร็จเรียบร้อย');
    } else {
        setAlert('error', 'เกิดข้อผิดพลาด ไม่สามารถสมัครสมาชิกได้');
    }

    redirect($page_path);
}
ob_start();
?>
<h1>สมัครสมาชิก</h1>
<h2>ระบบสำรองที่นั่งโรงภาพยนตร์</h2>

<?= showAlert() ?>
<form method="post">
    <label for="firstname">ชื่อ</label>
    <input type="text" name="firstname" id="firstname" required>
    <br>
    <label for="lastname">นามสกุล</label>
    <input type="text" name="lastname" id="lastname" required>
    <br>
    <label for="email">อีเมล</label>
    <input type="email" name="email" id="email" required>
    <br>
    <label for="password">รหัสผ่าน</label>
    <input type="password" name="password" id="password" required>
    <br> 
    <button type="submit">สมัคร</button>
</form>

<p>
    คุณมีบัญชีแล้ว? <a href="<?= url('/auth/login.php') ?>">เข้าสู่ระบบ</a>  
</p>
<?php
$layout_body = ob_get_clean();
$page_name = 'สมัครสมาชิก';
require INC . '/base_layout.php';