<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('admin');

$page_path = '/admin/reserves/request-list.php';

$items = db_result("SELECT * FROM `reserve_action` 
INNER JOIN `users` ON `users`.`user_id`=`reserve_action`.`user_id`
INNER JOIN `movie_times` ON `movie_times`.`movie_time_id`=`reserve_action`.`movie_time_id`
INNER JOIN `movies` ON `movies`.`movie_id`=`movie_times`.`movie_id`
WHERE `reserve_action`.`status`=1");

foreach ($items as &$item) {
    $seats = db_result("SELECT `theater_seats`.* FROM `reserve_items`
    LEFT JOIN `theater_seats` ON `theater_seats`.`theater_seat_id`=`reserve_items`.`theater_seat_id`
    WHERE `reserve_items`.`reserve_action_id`='{$item['reserve_action_id']}'");

    $seat_names = [];
    foreach ($seats as $seat) {
        $seat_names[] = $seat['seat_name'];
    }

    $item['seats'] = $seat_names;
    unset($item);
}

ob_start();
?>
<?= showAlert() ?>
<table>
    <thead>
        <tr>
            <th>รหัสการจอง</th>
            <th>ชื่อ</th>
            <th>สกุล</th>
            <th>อีเมล</th>
            <th>รหัสเวลาฉาย</th>
            <th>วันเวลาเริ่มฉาย</th>
            <th>รหัสภาพยนตร์</th>
            <th>ชื่อภาพยนตร์</th>
            <th>ที่นั้งที่จอง</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($items as $item) : ?>
        <tr>
            <td><?= $item['reserve_action_id'] ?></td>
            <td><?= $item['firstname'] ?></td>
            <td><?= $item['lastname'] ?></td>
            <td><?= $item['email'] ?></td>
            <td><?= $item['movie_time_id'] ?></td>
            <td><?= $item['start_time'] ?></td>
            <td><?= $item['movie_id'] ?></td>
            <td><?= $item['name'] ?></td>
            <td><?= implode(', ', $item['seats']) ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<?php
$layout_page = ob_get_clean();
$page_name = 'รายการอนุมัติการจองที่นั้งโรงภาพยนตร์';
require ROOT . '/admin/layout.php';
