<?php
require_once __DIR__ . '/../../boot.php';
checkAuth('user');

$id = get('id');
if (empty($id)) {
    redirect("/user/movies/search.php");
}

$page_path = "/user/movies/detail.php?id={$id}";

if (post()) {
    if (!post('seats')) {
        setAlert('error', "กรุณาเลือกที่นั้งโรงภาพยนตร์");
        redirect($page_path);
    }

    $qr_reserve = $db->query("INSERT INTO `reserve_action`(
    `user_id`, 
    `movie_time_id`, 
    `status`) VALUES (
    '{$user_id}',
    '{$_POST['movie_time']}',
    0)");

    if (!$qr_reserve) {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถจองได้");
        redirect($page_path);
    }

    $reserve_action_id = $db->insert_id;

    $seats = [];
    foreach (post('seats') as $seat) {
        $seats[] = "('{$reserve_action_id}', '{$seat}')";
    }

    $seats_sql = implode(',', $seats);

    $qr = $db->query("INSERT INTO `reserve_items`(
    `reserve_action_id`, 
    `theater_seat_id`) 
    VALUES {$seats_sql}");
    if ($qr) {
        setAlert('success', "จองที่นั้งโรงภาพยนตร์สำเร็จเรียบร้อย");
    } else {
        setAlert('error', "เกิดข้อผิดพลาด ไม่สามารถจองที่นั้งโรงภาพยนตร์ได้");
    }

    redirect($page_path);
}

$data = db_row("SELECT * FROM `movies` WHERE `movie_id`='{$id}'");
$times = db_result("SELECT * FROM `movie_times` WHERE `movie_id`='{$id}'");
$plan = db_row("SELECT * FROM `theater_plan` ORDER BY `id` DESC");
$seats = db_result("SELECT * FROM `theater_seats`");

ob_start();
?>
<?= showAlert() ?>

<img src="<?= url($data['poster']) ?>" alt="" style="
    max-width: 22rem;
">
<h3>ข้อมูลภาพยนตร์</h3>
<p>
    รหัสภาพยนตร์: <?= $data['movie_id'] ?>
    <br>
    ชื่อภาพยนตร์: <?= $data['name'] ?>
</p>

<form method="post">
    <h3>เวลาฉายภาพยนตร์</h3>
    <table>
        <thead>
            <tr>
                <th>รหัสเวลาฉาย</th>
                <th>วันเวลาเริ่มฉาย</th>
                <th>เลือกเวลาฉาย</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($times as $item) : ?>
            <tr>
                <td><?= $item['movie_time_id'] ?></td>
                <td><?= $item['start_time'] ?></td>
                <td>
                    <input type="radio" name="movie_time" id="movie_time<?= $item['movie_time_id'] ?>" value="<?= $item['movie_time_id'] ?>" required style="
                        transform: scale(1.5);
                    ">
                    <label for="movie_time<?= $item['movie_time_id'] ?>">เลือก</label>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <h3>ผังที่นั้งโรงภาพยนตร์</h3>
    <img src="<?= url($plan['img']) ?>" alt="" style="
        max-width: 22rem;
    ">

    <table>
        <thead>
            <tr>
                <th>รหัส</th>
                <th>ชื่อที่นั้ง</th>
                <th>เลือกที่นั้ง</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($seats as $item) : ?>
            <tr>
                <td><?= $item['theater_seat_id'] ?></td>
                <td><?= $item['seat_name'] ?></td>
                <td>
                    <input type="checkbox" name="seats[]" id="seat<?= $item['theater_seat_id'] ?>" value="<?= $item['theater_seat_id'] ?>" style="
                        transform: scale(1.5);
                    ">
                    <label for="seat<?= $item['theater_seat_id'] ?>">เลือก</label>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <br>
    <button type="submit">
        จองที่นั้งโรงภาพยนตร์
    </button>
</form>

<?php
$layout_page = ob_get_clean();
$page_name = 'รายละเอียดภาพยนตร์';
require ROOT . '/user/layout.php';
